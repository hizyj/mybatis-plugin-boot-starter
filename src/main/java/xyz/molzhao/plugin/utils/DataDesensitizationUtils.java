package xyz.molzhao.plugin.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * 加密工具
 */
public class DataDesensitizationUtils {
    private static final Logger log = LoggerFactory.getLogger(DataDesensitizationUtils.class);

    /**
     * 加密
     * @param data
     * @return
     * @throws Exception
     */
    public static String encrypt(String data, String key) {
        try {
            byte[] plaintext = data.getBytes();
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.substring(0, 16).getBytes(), "AES"), new IvParameterSpec(key.substring(16).getBytes()));
            byte[] encrypted = cipher.doFinal(plaintext);
            return new BASE64Encoder().encode(encrypted).trim();
        } catch (Exception e) {
            log.error("加密数据失败", e);
            return data;
        }
    }

    /**
     * 解密
     * @param data
     * @return
     */
    public static String decrypt(String data, String key) {
        try {
            byte[] encrypted = new BASE64Decoder().decodeBuffer(data);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.substring(0, 16).getBytes(), "AES"), new IvParameterSpec(key.substring(16).getBytes()));
            byte[] original = cipher.doFinal(encrypted);
            String originalString = new String(original);
            return originalString.trim();
        } catch (Exception e) {
            log.error("解密数据失败", e);
            return data;
        }
    }
}
