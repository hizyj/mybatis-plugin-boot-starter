package xyz.molzhao.plugin.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@ConfigurationProperties(prefix = MybatisPluginConstant.MYBATIS_PREFIX)
public class MybatisPluginProperties {
    private boolean masterSlaveAutoRouting = false;

    @NestedConfigurationProperty
    private EncryptProperties encrypt = new EncryptProperties();

    public boolean isMasterSlaveAutoRouting() {
        return masterSlaveAutoRouting;
    }

    public void setMasterSlaveAutoRouting(boolean masterSlaveAutoRouting) {
        this.masterSlaveAutoRouting = masterSlaveAutoRouting;
    }

    public EncryptProperties getEncrypt() {
        return encrypt;
    }

    public void setEncrypt(EncryptProperties encrypt) {
        this.encrypt = encrypt;
    }

}
