package xyz.molzhao.plugin.properties;

public class MybatisPluginConstant {
    private MybatisPluginConstant() {

    }

    public static final String MYBATIS_PREFIX = "mybatis-plugin";

    public static final String MYBATIS_MYBATIS_ENCRYPT_PLUGIN_PREFIX = "mybatis-plugin.encrypt";

    public static final String MYBATIS_MASTER_SLAVE_AUTO_ROUTING = "masterSlaveAutoEnable";

    public static final String MYBATIS_ENCRYPT_PLUGIN_ENABLE = "encrypt.enable";
}
