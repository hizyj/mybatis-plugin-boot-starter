package xyz.molzhao.plugin.properties;

public class EncryptProperties {
    private boolean enable = false;
    // 默认加密key
    private String encryptKey = "8ce87b8aa3463f4561635f66991592ae";

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getEncryptKey() {
        return encryptKey;
    }

    public void setEncryptKey(String encryptKey) {
        this.encryptKey = encryptKey;
    }
}
