package xyz.molzhao.plugin.crypt;

import java.util.HashMap;
import java.util.Map;

public class EncryptContext {
    private static final Map<EncryptTypeEnum, Encrypt> Encrypts = new HashMap<>(EncryptTypeEnum.values().length);

    private EncryptContext() {

    }

    /**
     * 获取加密方式
     *
     * @param encryptTypeEnum
     *            加密方式枚举
     * @return 机密方式实现类
     */
    public static Encrypt getEncrypt(EncryptTypeEnum encryptTypeEnum) {
        Encrypt crypt = Encrypts.get(encryptTypeEnum);
        if (crypt == null) {
            crypt = Encrypts.get(EncryptTypeEnum.AES);
        }

        return crypt;
    }

    /**
     * 设置加密方式
     *
     * @param encryptTypeEnum
     *            加密类型
     * @param crypt
     *            加载方式
     */
    public static void setEncrypt(EncryptTypeEnum encryptTypeEnum, Encrypt crypt) {
        Encrypts.put(encryptTypeEnum, crypt);
    }
}
