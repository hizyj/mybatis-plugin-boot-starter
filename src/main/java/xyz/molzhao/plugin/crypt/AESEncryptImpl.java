package xyz.molzhao.plugin.crypt;

import xyz.molzhao.plugin.properties.MybatisPluginProperties;
import xyz.molzhao.plugin.utils.DataDesensitizationUtils;

public class AESEncryptImpl implements Encrypt {
    private final MybatisPluginProperties mybatisPluginProperties;

    public AESEncryptImpl(MybatisPluginProperties mybatisPluginProperties) {
        this.mybatisPluginProperties = mybatisPluginProperties;
    }

    @Override
    public String encrypt(String plain) {
        return DataDesensitizationUtils.encrypt(plain, mybatisPluginProperties.getEncrypt().getEncryptKey());
    }

    @Override
    public String decrypt(String cipher) {
        return DataDesensitizationUtils.decrypt(cipher, mybatisPluginProperties.getEncrypt().getEncryptKey());
    }
}
