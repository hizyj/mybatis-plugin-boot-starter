package xyz.molzhao.plugin.crypt;

import xyz.molzhao.plugin.properties.MybatisPluginProperties;

public class EncryptLoader {

    /**
     * 加载所有加密方式实现类
     */

    public void loadCrypt(MybatisPluginProperties properties) {
        EncryptContext.setEncrypt(EncryptTypeEnum.AES, new AESEncryptImpl(properties));
    }
}
