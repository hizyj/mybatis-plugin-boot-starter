package xyz.molzhao.plugin.crypt;

public interface Encrypt {
    String encrypt(String plain);

    /**
     * 解密
     *
     * @param cipher
     *            密文
     * @return 原始明文
     */
    String decrypt(String cipher);
}
