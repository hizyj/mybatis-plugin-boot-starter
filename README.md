# mybatis-plugin-boot-starter

#### 介绍
基于mybatis拦截器形式
1、实现数据库脱敏，数据库密文，程序中明文，超简配置
2、数据库读写分离

#### 安装教程
```xml
<dependency>
    <groupId>xyz.molzhao</groupId>
    <artifactId>mybatis-plugin-boot-starter</artifactId>
    <version>1.0.1</version>
</dependency>
```

#### 使用说明
本项目依赖:
1. dynamic-datasource-spring-boot-starter
2. mybatis-plus-boot-starter（可替换mybatis）

默认主库名：master

默认从库名：salve

```yaml
spring:
  datasource:
    dynamic:
      datasource:
        master:
          username: username
          password: password
          url: jdbc:mysql://127.0.0.1:3306/dbName?rewriteBatchedStatements=true&useUnicode=true&characterEncoding=UTF8&useSSL=false&allowMultiQueries=true
          driver-class-name: com.mysql.jdbc.Driver
        salve:
          username: username
          password: password
          url: jdbc:mysql://127.0.0.1:3306/mybatis-plus?rewriteBatchedStatements=true&useUnicode=true&characterEncoding=UTF8&useSSL=false&allowMultiQueries=true
          driver-class-name: com.mysql.jdbc.Driver

mybatis-plugin:
# 开启读写分离插件
  masterSlaveAutoEnable: true
  encrypt:
# 开启加密插件
    enable: false
# 自定义加密的key
    encryptKey: 8ce87b8aa3463f4561635f66991592aa
```


#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
